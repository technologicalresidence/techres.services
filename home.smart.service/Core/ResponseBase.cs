namespace home.smart.service.Core
{
    public class ResponseBase
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public bool Enabled { get; set; }
        public string ValidationErrorMessage { get; set; }

        public bool HasValidationError()
        {
            return !string.IsNullOrWhiteSpace(ValidationErrorMessage);
        }
    }
}