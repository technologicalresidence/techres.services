namespace home.smart.service.Core
{
    public class RequestBase
    {
        public int Id { get; set; }
        public string User { get; set; }
        public string Role { get; set; }
        public string Transaction { get; set; }
    }
}