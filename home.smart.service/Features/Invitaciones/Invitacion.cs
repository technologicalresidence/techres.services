using System;
using System.ComponentModel.DataAnnotations.Schema;
using home.smart.service.Core;
using home.smart.service.Features.Residentes;

namespace home.smart.service.Features.Invitaciones
{
    [Table("Invitaciones")]
    public class Invitacion : Entity
    {
       public string CodigoInvitacion { get; private set; }
       public string Invitado { get; private set; }
       public string NumeroTelefono { get; private set; }
       public string IdentidadDelInvitado { get; private set; }
       public int ResidenteId { get; private set; }
       public int VigenciaMinutos { get; private set; }
       public string EstadoDeVigencia { get; private set; } 

       public Residente Residente { get; set; }

        public void Update(
             string codigoInvitacion, string invitado, string numeroTelefono, 
             string identidadInvitado, int residenteId, int vigenciaMinutos, string estadoVigencia, string user)
        {
            CodigoInvitacion = codigoInvitacion;
            Invitado = invitado;
            NumeroTelefono = numeroTelefono;
            IdentidadDelInvitado = identidadInvitado;
            ResidenteId = residenteId;
            VigenciaMinutos = vigenciaMinutos;
            EstadoDeVigencia = estadoVigencia;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModificarInvitacion";
            ModifiedBy = user;
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public class Builder
        {
            private readonly Invitacion _invitacion = new Invitacion();

            public Builder ConCodigoInvitacion(string codigoInvitacion)
            {
                _invitacion.CodigoInvitacion = codigoInvitacion;
                return this;
            }

            public Builder ConInvitado(string invitado)
            {
                _invitacion.Invitado = invitado;
                return this;
            }

            public Builder ConIndentidadDelInvitado(string identidadDelInvitado)
            {
                _invitacion.IdentidadDelInvitado = identidadDelInvitado;
                return this;
            }

            public Builder ConTelefono(string numeroTelefono)
            {
                _invitacion.NumeroTelefono = numeroTelefono;
                return this;
            }

            public Builder ConResidente(int residente)
            {
                _invitacion.ResidenteId = residente;
                return this;
            }

            public Builder ConVigencia(int minutos)
            {
                _invitacion.VigenciaMinutos = minutos;
                return this;
            }

            public Builder ConEstadoVigencia(string estadoVigencia)
            {
                _invitacion.EstadoDeVigencia = estadoVigencia;
                return this;
            }

            public Builder WithAuditFields(string user)
            {
                _invitacion.CrudOperation = "Added";
                _invitacion.TransactionDate = DateTime.Now;
                _invitacion.TransactionType = "CrearResidente";
                _invitacion.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _invitacion.TransactionUId = Guid.NewGuid();
                _invitacion.Enabled = true;

                return this;
            }

            public Invitacion Build()
            {
                return _invitacion;
            }
        }
    }
}