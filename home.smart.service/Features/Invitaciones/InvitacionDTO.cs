using home.smart.service.Core;

namespace home.smart.service.Features.Invitaciones
{
    public class InvitacionDTO : ResponseBase
    {
        public string CodigoInvitacion { get; set; }
        public string Invitado { get; set; }
        public string NumeroTelefono { get;  set; }
        public string IdentidadDelInvitado { get; set; }
        public int ResidenteId { get; set; }
        public int VigenciaMinutos { get; set; }
        public string EstadoDeVigencia { get; set; }
    }
}