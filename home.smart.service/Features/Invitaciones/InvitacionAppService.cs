using System;
using System.Linq;
using home.smart.service.Core;
using home.smart.service.DataContext;

namespace home.smart.service.Features.Invitaciones
{
    public class InvitacionAppService : IInvitacionAppService
    {
        private SmartHomeContext _context;
        private readonly IInvitacionDomainService _invitacionDomainService;

        public InvitacionAppService(SmartHomeContext context, IInvitacionDomainService localidadDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (localidadDomainService == null) throw new ArgumentException(nameof(localidadDomainService));

            _context = context;
            _invitacionDomainService = localidadDomainService;
        }
        public InvitacionDTO Create(InvitacionRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newInvitado = _invitacionDomainService.Create(request);

            _context.Invitaciones.Add(newInvitado);
            _context.SaveChanges();

            return new InvitacionDTO
            {
                CodigoInvitacion = newInvitado.CodigoInvitacion,
                Invitado = newInvitado.Invitado,
                IdentidadDelInvitado = newInvitado.IdentidadDelInvitado,
                NumeroTelefono = newInvitado.NumeroTelefono,
                ResidenteId = newInvitado.ResidenteId,
                VigenciaMinutos = newInvitado.VigenciaMinutos,
                EstadoDeVigencia = newInvitado.EstadoDeVigencia,
                Enabled = newInvitado.Enabled
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var zona = _context.Invitaciones.FirstOrDefault(s => s.Id == id);
            if (zona == null) throw new Exception("Error al intentar obtener país");
            _context.Invitaciones.Remove(zona);
            _context.SaveChanges();

            return string.Empty;
        }

        public InvitacionPagedDTO GetPaged(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Invitaciones.Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Invitaciones
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new InvitacionPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new InvitacionDTO
                    {
                        Id = s.Id,
                        CodigoInvitacion = s.CodigoInvitacion,
                        Invitado = s.Invitado,
                        IdentidadDelInvitado = s.IdentidadDelInvitado,
                        NumeroTelefono = s.NumeroTelefono,
                        ResidenteId = s.ResidenteId,
                        VigenciaMinutos = s.VigenciaMinutos,
                        EstadoDeVigencia = s.EstadoDeVigencia,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.Invitaciones.Where(s => s.CodigoInvitacion.Contains(request.Value)).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Invitaciones.Where(s => s.CodigoInvitacion.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new InvitacionPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = lista.Select(s => new InvitacionDTO
                    {
                        Id = s.Id,
                        CodigoInvitacion = s.CodigoInvitacion,
                        Invitado = s.Invitado,
                        IdentidadDelInvitado = s.IdentidadDelInvitado,
                        NumeroTelefono = s.NumeroTelefono,
                        ResidenteId = s.ResidenteId,
                        VigenciaMinutos = s.VigenciaMinutos,
                        EstadoDeVigencia = s.EstadoDeVigencia,
                    }).ToList()
                };
            }
        }

        public InvitacionDTO Update(InvitacionRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldzonaInfo = _context.Invitaciones.FirstOrDefault(s => s.Id == request.Id);
            if (oldzonaInfo == null) return new InvitacionDTO { ValidationErrorMessage = "Error al obtener info de País" };

            var rolUpdate = _invitacionDomainService.Update(request, oldzonaInfo);

            _context.Invitaciones.Update(oldzonaInfo);
            _context.SaveChanges();

            return new InvitacionDTO
            {
                Id = oldzonaInfo.Id,
                CodigoInvitacion = oldzonaInfo.CodigoInvitacion,
                Invitado = oldzonaInfo.Invitado,
                IdentidadDelInvitado = oldzonaInfo.IdentidadDelInvitado,
                NumeroTelefono = oldzonaInfo.NumeroTelefono,
                ResidenteId = oldzonaInfo.ResidenteId,
                VigenciaMinutos = oldzonaInfo.VigenciaMinutos,
                EstadoDeVigencia = oldzonaInfo.EstadoDeVigencia,
                Enabled = oldzonaInfo.Enabled
            };
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_invitacionDomainService != null) _invitacionDomainService.Dispose();
        }
    }
}