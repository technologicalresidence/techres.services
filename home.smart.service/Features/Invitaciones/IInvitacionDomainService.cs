using System;

namespace home.smart.service.Features.Invitaciones
{
    public interface IInvitacionDomainService : IDisposable
    {
        Invitacion Create(InvitacionRequest request);
        Invitacion Update(InvitacionRequest request, Invitacion _oldRegister);
    }
}