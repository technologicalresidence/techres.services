using System;
using home.smart.service.Core;

namespace home.smart.service.Features.Invitaciones
{
    public interface IInvitacionAppService : IDisposable
    {
        InvitacionPagedDTO GetPaged(PagedGeneralRequest request);
        InvitacionDTO Create(InvitacionRequest request);
        InvitacionDTO Update(InvitacionRequest request);
        string Delete(int id);
    }
}