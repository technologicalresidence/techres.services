using System.Collections.Generic;
using home.smart.service.Core;

namespace home.smart.service.Features.Invitaciones
{
    public class InvitacionPagedDTO : ResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<InvitacionDTO> Items { get; set; }
    }
}