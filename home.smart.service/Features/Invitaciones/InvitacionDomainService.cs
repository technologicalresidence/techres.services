using System;

namespace home.smart.service.Features.Invitaciones
{
    public class InvitacionDomainService : IInvitacionDomainService
    {
        public Invitacion Create(InvitacionRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.CodigoInvitacion)) throw new ArgumentNullException(nameof(request.CodigoInvitacion));

            Invitacion zona = new Invitacion.Builder()
            .ConCodigoInvitacion(request.CodigoInvitacion)
            .ConInvitado(request.Invitado)
            .ConIndentidadDelInvitado(request.IdentidadDelInvitado)
            .ConTelefono(request.NumeroTelefono)
            .ConResidente(request.ResidenteId)
            .ConVigencia(request.VigenciaMinutos)
            .ConEstadoVigencia(request.EstadoDeVigencia)
            .Build();

            return zona;
        }

        public Invitacion Update(InvitacionRequest request, Invitacion _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(
                request.CodigoInvitacion, request.Invitado, request.NumeroTelefono, 
                request.IdentidadDelInvitado, request.ResidenteId, request.VigenciaMinutos, request.EstadoDeVigencia, request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        }
    }
}