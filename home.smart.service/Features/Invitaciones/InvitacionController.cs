using System;
using home.smart.service.Core;
using Microsoft.AspNetCore.Mvc;

namespace home.smart.service.Features.Invitaciones
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvitacionController : ControllerBase
    {
        private readonly IInvitacionAppService _invitacionAppService;
        public InvitacionController(IInvitacionAppService invitacionAppService)
        {
            if (invitacionAppService == null) throw new ArgumentException(nameof(invitacionAppService));

            _invitacionAppService = invitacionAppService;
        }
        [HttpGet]
        [Route("paged")]
        // [Authorize]
        public ActionResult<InvitacionPagedDTO> GetPaged([FromQuery]PagedGeneralRequest request)
        {
            return Ok(_invitacionAppService.GetPaged(request));
        }

        [HttpPost]
        // [Authorize]
        public ActionResult<InvitacionDTO> Post([FromBody]InvitacionRequest request)
        {
            return Ok(_invitacionAppService.Create(request));
        }

        [HttpPut]
        // [Authorize]
        public ActionResult<InvitacionDTO> Put([FromBody] InvitacionRequest request)
        {
            return Ok(_invitacionAppService.Update(request));
        }

        [HttpDelete]
        // [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_invitacionAppService.Delete(Id));
        }
    }
}