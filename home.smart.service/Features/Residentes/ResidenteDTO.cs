using home.smart.service.Core;

namespace home.smart.service.Features.Residentes
{
    public class ResidenteDTO : ResponseBase
    {
        public string Nombres { get;  set; }
        public string Appellido { get;  set; }
        public string Telefono { get;  set; }
        public string Identidad { get;  set; }
        public string Correo { get;  set; }
        public int LocalidadId { get;  set; }
    }
}