using System;
using home.smart.service.Core;

namespace home.smart.service.Features.Residentes
{
    public interface IResidenteAppService : IDisposable
    {
        ResidentePagedDTO GetPaged(PagedGeneralRequest request);
        ResidenteDTO Create(ResidenteRequest request);
        ResidenteDTO Update(ResidenteRequest request);
        string Delete(int id);
    }
}