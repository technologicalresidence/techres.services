using System;
using home.smart.service.Core;
using Microsoft.AspNetCore.Mvc;

namespace home.smart.service.Features.Residentes
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResidenteController : ControllerBase
    {
        private readonly IResidenteAppService _residenteAppService;
        public ResidenteController(IResidenteAppService countryAppService)
        {
            if (countryAppService == null) throw new ArgumentException(nameof(countryAppService));

            _residenteAppService = countryAppService;
        }
        [HttpGet]
        [Route("paged")]
        // [Authorize]
        public ActionResult<ResidentePagedDTO> GetPaged([FromQuery]PagedGeneralRequest request)
        {
            return Ok(_residenteAppService.GetPaged(request));
        }

        [HttpPost]
        // [Authorize]
        public ActionResult<ResidenteDTO> Post([FromBody]ResidenteRequest request)
        {
            return Ok(_residenteAppService.Create(request));
        }

        [HttpPut]
        // [Authorize]
        public ActionResult<ResidenteDTO> Put([FromBody] ResidenteRequest request)
        {
            return Ok(_residenteAppService.Update(request));
        }

        [HttpDelete]
        // [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_residenteAppService.Delete(Id));
        }
    }
}