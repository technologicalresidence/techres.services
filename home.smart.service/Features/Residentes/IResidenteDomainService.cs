using System;

namespace home.smart.service.Features.Residentes
{
    public interface IResidenteDomainService : IDisposable
    {
        Residente Create(ResidenteRequest request);
        Residente Update(ResidenteRequest request, Residente _oldRegister);
    }
}