using System.Collections.Generic;

namespace home.smart.service.Features.Residentes
{
    public class ResidentePagedDTO
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<ResidenteDTO> Items { get; set; }
    }
}