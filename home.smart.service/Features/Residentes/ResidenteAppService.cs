using System;
using System.Linq;
using home.smart.service.Core;
using home.smart.service.DataContext;

namespace home.smart.service.Features.Residentes
{
    public class ResidenteAppService : IResidenteAppService
    {
        private SmartHomeContext _context;
        private readonly IResidenteDomainService _residenteDomainService;

        public ResidenteAppService(SmartHomeContext context, IResidenteDomainService countryDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (countryDomainService == null) throw new ArgumentException(nameof(countryDomainService));

            _context = context;
            _residenteDomainService = countryDomainService;
        }
        public ResidenteDTO Create(ResidenteRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newResidente = _residenteDomainService.Create(request);

            _context.Residentes.Add(newResidente);
            _context.SaveChanges();

            return new ResidenteDTO
            {
                Nombres = newResidente.Nombres,
                Appellido = newResidente.Appellido,
                Telefono = newResidente.Telefono,
                Identidad = newResidente.Identidad,
                Correo = newResidente.Correo,
                LocalidadId = newResidente.LocalidadId,
                Enabled = newResidente.Enabled
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var country = _context.Residentes.FirstOrDefault(s => s.Id == id);
            if (country == null) throw new Exception("Error al intentar obtener residente");
            _context.Residentes.Remove(country);
            _context.SaveChanges();

            return string.Empty;
        }

        public ResidentePagedDTO GetPaged(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Residentes.Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Residentes
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ResidentePagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new ResidenteDTO
                    {
                        Id = s.Id,
                        Nombres = s.Nombres,
                        Appellido = s.Appellido,
                        Telefono = s.Telefono,
                        Identidad = s.Identidad,
                        Correo = s.Correo,
                        LocalidadId = s.LocalidadId,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.Residentes.Where(s => s.Nombres.Contains(request.Value)).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Residentes.Where(s => s.Nombres.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ResidentePagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = lista.Select(s => new ResidenteDTO
                    {
                        Id = s.Id,
                        Nombres = s.Nombres,
                        Appellido = s.Appellido,
                        Telefono = s.Telefono,
                        Identidad = s.Identidad,
                        Correo = s.Correo,
                        LocalidadId = s.LocalidadId,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
        }

        public ResidenteDTO Update(ResidenteRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldCountryInfo = _context.Residentes.FirstOrDefault(s => s.Id == request.Id);
            if (oldCountryInfo == null) return new ResidenteDTO { ValidationErrorMessage = "Error al obtener info de residente" };

            var rolUpdate = _residenteDomainService.Update(request, oldCountryInfo);

            _context.Residentes.Update(oldCountryInfo);
            _context.SaveChanges();

            return new ResidenteDTO
            {
                Id = oldCountryInfo.Id,
                Nombres = oldCountryInfo.Nombres,
                Appellido = oldCountryInfo.Appellido,
                Telefono = oldCountryInfo.Telefono,
                Identidad = oldCountryInfo.Identidad,
                Correo = oldCountryInfo.Correo,
                LocalidadId = oldCountryInfo.LocalidadId,
                Enabled = oldCountryInfo.Enabled
            };
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_residenteDomainService != null) _residenteDomainService.Dispose();
        }
    }
}