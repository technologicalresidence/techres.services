using System;

namespace home.smart.service.Features.Residentes
{
    public class ResidenteDomainService : IResidenteDomainService
    {
        public Residente Create(ResidenteRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Nombres)) throw new ArgumentNullException(nameof(request.Nombres));

            Residente Residente = new Residente.Builder()
            .ConNombres(request.Nombres)
            .ConApellidos(request.Appellido)
            .ConTelefono(request.Telefono)
            .ConIdentidad(request.Identidad)
            .ConCorreo(request.Correo)
            .ConLocalidadId(request.LocalidadId)
            .WithAuditFields(request.User)
            .Build();

            return Residente;
        }

        public Residente Update(ResidenteRequest request, Residente _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(
                request.Nombres, request.Appellido, request.Telefono, 
                request.Identidad, request.Correo, request.LocalidadId,  request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        }
    }
}