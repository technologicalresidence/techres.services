using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using home.smart.service.Core;
using home.smart.service.Features.Invitaciones;
using home.smart.service.Features.Localidades;

namespace home.smart.service.Features.Residentes
{
    [Table("Residentes")]
    public class Residente : Entity
    {
        public string Nombres { get; private set; }
        public string Appellido { get; private set; }  
        public string Telefono { get; private set; }
        public string Identidad { get; private set; }
        public string Correo { get; private set; }
        public int LocalidadId { get; private set; }
        public Localidad Localidad { get; set; }
        public ICollection<Invitacion> Invitaciones { get; set; }

        public void Update(
            string nombres, string apellidos, string telefono, string identidad, string correo, int localidadId, string user)
        {
            Nombres = nombres;
            Appellido = apellidos;
            Telefono = telefono;
            Identidad = identidad;
            Correo = Correo;
            LocalidadId = localidadId;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModificarResidente";
            ModifiedBy = user;
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public class Builder
        {
            private readonly Residente _residente = new Residente();

            public Builder ConNombres(string nombres)
            {
                _residente.Nombres = nombres;
                return this;
            }

            public Builder ConApellidos(string apellidos)
            {
                _residente.Appellido = apellidos;
                return this;
            }

            public Builder ConTelefono(string telefono)
            {
                _residente.Telefono = telefono;
                return this;
            }

            public Builder ConIdentidad(string identidad)
            {
                _residente.Identidad = identidad;
                return this;
            }

            public Builder ConCorreo(string correo)
            {
                _residente.Correo = correo;
                return this;
            }

            public Builder ConLocalidadId(int localidadId)
            {
                _residente.LocalidadId = localidadId;
                return this;
            }

            public Builder WithAuditFields(string user)
            {
                _residente.CrudOperation = "Added";
                _residente.TransactionDate = DateTime.Now;
                _residente.TransactionType = "CrearResidente";
                _residente.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _residente.TransactionUId = Guid.NewGuid();
                _residente.Enabled = true;

                return this;
            }

            public Residente Build()
            {
                return _residente;
            }
        }
     
    }
}