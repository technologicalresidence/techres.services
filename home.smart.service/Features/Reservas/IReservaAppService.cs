using System;
using home.smart.service.Core;

namespace home.smart.service.Features.Reservas
{
    public interface IReservaAppService : IDisposable
    {
        ReservaPagedDTO GetPaged(PagedGeneralRequest request);
        ReservaDTO Create(ReservaRequest request);
        ReservaDTO Update(ReservaRequest request);
        string Delete(int id);
    }
}