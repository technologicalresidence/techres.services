using System;
using System.ComponentModel.DataAnnotations.Schema;
using home.smart.service.Core;
using home.smart.service.Features.LocalidadesComunitarias;

namespace home.smart.service.Features.Reservas
{
    [Table("Reservas")]
    public class Reserva : Entity
    {
       public string CodigoReserva { get; private set; }
       public int ResidenteId { get; private set; }
       public int VigenciaMinutos { get; private set; }
       public string EstadoVigencia { get; private set; }
       public int LocalidadComunitariaId { get; private set; }

       public LocalidadComunitaria LocalidadComunitaria { get; set; }

        public void Update(
              string codigoreserva, int residenteId, int vigenciaMinutos, string estadoVigencia, int localidadComunitariaId, string user)
        {
            CodigoReserva = codigoreserva;
            ResidenteId = residenteId;
            VigenciaMinutos = vigenciaMinutos;
            EstadoVigencia = estadoVigencia;
            LocalidadComunitariaId = localidadComunitariaId;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModificarReserva";
            ModifiedBy = user;
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public class Builder
        {
            private readonly Reserva _reserva = new Reserva();

            public Builder ConCodigoReserva(string codigoReserva)
            {
                _reserva.CodigoReserva = codigoReserva;
                return this;
            }

            public Builder ConResidente(int residente)
            {
                _reserva.ResidenteId = residente;
                return this;
            }

            public Builder ConVigencia(int minutos)
            {
                _reserva.VigenciaMinutos = minutos;
                return this;
            }

            public Builder ConLocalidadComunitaria(int localidadComunitariaId)
            {
                _reserva.LocalidadComunitariaId = localidadComunitariaId;
                return this;
            }

            public Builder ConEstadoVigencia(string estadoVigencia)
            {
                _reserva.EstadoVigencia = estadoVigencia;
                return this;
            }

            public Builder WithAuditFields(string user)
            {
                _reserva.CrudOperation = "Added";
                _reserva.TransactionDate = DateTime.Now;
                _reserva.TransactionType = "CrearReserva";
                _reserva.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _reserva.TransactionUId = Guid.NewGuid();
                _reserva.Enabled = true;

                return this;
            }

            public Reserva Build()
            {
                return _reserva;
            }
        }
    }
}