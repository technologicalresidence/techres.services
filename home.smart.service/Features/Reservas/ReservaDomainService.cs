using System;

namespace home.smart.service.Features.Reservas
{
    public class ReservaDomainService : IReservaDomainService
    {
        public Reserva Create(ReservaRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.CodigoReserva)) throw new ArgumentNullException(nameof(request.CodigoReserva));

            Reserva zona = new Reserva.Builder()
            .ConCodigoReserva(request.CodigoReserva)
            .ConResidente(request.ResidenteId)
            .ConVigencia(request.VigenciaMinutos)
            .ConEstadoVigencia(request.EstadoVigencia)
            .ConLocalidadComunitaria(request.LocalidadComunitariaId)
            .Build();

            return zona;
        }

        public Reserva Update(ReservaRequest request, Reserva _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(
                request.CodigoReserva, request.ResidenteId, request.VigenciaMinutos, request.EstadoVigencia, request.LocalidadComunitariaId, request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        } 
    }
}