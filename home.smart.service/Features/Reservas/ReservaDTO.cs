using home.smart.service.Core;

namespace home.smart.service.Features.Reservas
{
    public class ReservaDTO : ResponseBase
    {
        public string CodigoReserva { get; set; }
        public int ResidenteId { get; set; }
        public int VigenciaMinutos { get; set; }
        public string EstadoVigencia { get; set; }
        public int LocalidadComunitariaId { get; set; }
    }
}