using System;
using home.smart.service.Core;
using Microsoft.AspNetCore.Mvc;

namespace home.smart.service.Features.Reservas
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReservaController : ControllerBase
    {
        private readonly IReservaAppService _reservaAppService;
        public ReservaController(IReservaAppService reservaAppService)
        {
            if (reservaAppService == null) throw new ArgumentException(nameof(reservaAppService));

            _reservaAppService = reservaAppService;
        }
        [HttpGet]
        [Route("paged")]
        // [Authorize]
        public ActionResult<ReservaPagedDTO> GetPaged([FromQuery]PagedGeneralRequest request)
        {
            return Ok(_reservaAppService.GetPaged(request));
        }

        [HttpPost]
        // [Authorize]
        public ActionResult<ReservaDTO> Post([FromBody]ReservaRequest request)
        {
            return Ok(_reservaAppService.Create(request));
        }

        [HttpPut]
        // [Authorize]
        public ActionResult<ReservaDTO> Put([FromBody] ReservaRequest request)
        {
            return Ok(_reservaAppService.Update(request));
        }

        [HttpDelete]
        // [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_reservaAppService.Delete(Id));
        } 
    }
}