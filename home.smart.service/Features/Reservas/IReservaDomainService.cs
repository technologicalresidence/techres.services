using System;
using home.smart.service.Core;

namespace home.smart.service.Features.Reservas
{
    public interface IReservaDomainService : IDisposable
    {
        Reserva Create(ReservaRequest request);
        Reserva Update(ReservaRequest request, Reserva _oldRegister);
    }
}