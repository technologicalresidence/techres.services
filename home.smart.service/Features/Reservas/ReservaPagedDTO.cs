using System.Collections.Generic;
using home.smart.service.Core;

namespace home.smart.service.Features.Reservas
{
    public class ReservaPagedDTO : ResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<ReservaDTO> Items { get; set; }
    }
}