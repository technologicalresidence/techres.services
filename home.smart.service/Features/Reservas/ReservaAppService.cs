using System;
using System.Linq;
using home.smart.service.Core;
using home.smart.service.DataContext;

namespace home.smart.service.Features.Reservas
{
    public class ReservaAppService : IReservaAppService
    {
        private SmartHomeContext _context;
        private readonly IReservaDomainService _reservaDomainService;

        public ReservaAppService(SmartHomeContext context, IReservaDomainService localidadDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (localidadDomainService == null) throw new ArgumentException(nameof(localidadDomainService));

            _context = context;
            _reservaDomainService = localidadDomainService;
        }
        public ReservaDTO Create(ReservaRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newReserva = _reservaDomainService.Create(request);

            _context.Reservas.Add(newReserva);
            _context.SaveChanges();

            return new ReservaDTO
            {
                CodigoReserva = newReserva.CodigoReserva,
                ResidenteId = newReserva.ResidenteId,
                VigenciaMinutos = newReserva.VigenciaMinutos,
                EstadoVigencia = newReserva.EstadoVigencia,
                LocalidadComunitariaId = newReserva.LocalidadComunitariaId,
                Enabled = newReserva.Enabled
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var zona = _context.Reservas.FirstOrDefault(s => s.Id == id);
            if (zona == null) throw new Exception("Error al intentar obtener país");
            _context.Reservas.Remove(zona);
            _context.SaveChanges();

            return string.Empty;
        }

        public ReservaPagedDTO GetPaged(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Reservas.Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Reservas
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ReservaPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new ReservaDTO
                    {
                        Id = s.Id,
                        CodigoReserva = s.CodigoReserva,
                        ResidenteId = s.ResidenteId,
                        VigenciaMinutos = s.VigenciaMinutos,
                        EstadoVigencia = s.EstadoVigencia,
                        LocalidadComunitariaId = s.LocalidadComunitariaId,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.Reservas.Where(s => s.CodigoReserva.Contains(request.Value)).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Reservas.Where(s => s.CodigoReserva.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ReservaPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = lista.Select(s => new ReservaDTO
                    {
                        Id = s.Id,
                        CodigoReserva = s.CodigoReserva,
                        ResidenteId = s.ResidenteId,
                        VigenciaMinutos = s.VigenciaMinutos,
                        EstadoVigencia = s.EstadoVigencia,
                        LocalidadComunitariaId = s.LocalidadComunitariaId
                    }).ToList()
                };
            }
        }

        public ReservaDTO Update(ReservaRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldReservaInfo = _context.Reservas.FirstOrDefault(s => s.Id == request.Id);
            if (oldReservaInfo == null) return new ReservaDTO { ValidationErrorMessage = "Error al obtener info de País" };

            var rolUpdate = _reservaDomainService.Update(request, oldReservaInfo);

            _context.Reservas.Update(oldReservaInfo);
            _context.SaveChanges();

            return new ReservaDTO
            {
                Id = oldReservaInfo.Id,
                CodigoReserva = oldReservaInfo.CodigoReserva,
                ResidenteId = oldReservaInfo.ResidenteId,
                VigenciaMinutos = oldReservaInfo.VigenciaMinutos,
                EstadoVigencia = oldReservaInfo.EstadoVigencia,
                LocalidadComunitariaId = oldReservaInfo.LocalidadComunitariaId,
                Enabled = oldReservaInfo.Enabled
            };
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_reservaDomainService != null) _reservaDomainService.Dispose();
        }
    }
}