using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using home.smart.service.Core;
using home.smart.service.Features.Reservas;
using home.smart.service.Features.Zones;

namespace home.smart.service.Features.LocalidadesComunitarias
{
    [Table("LocalidadesComunitarias")]
    public class LocalidadComunitaria : Entity
    {
        public string Nombre { get; private set; }
        public string Ubicacion { get; private set; }
        public int ZonaId { get; private set; }
        public Zona Zona { get; set; }
        public ICollection<Reserva> Reservas { get; set; }

        public void Update(
            string _nombre, string _ubicacion, int zonaId, string _user)
        {
            Nombre = _nombre;
            Ubicacion = _ubicacion;
            ZonaId = zonaId;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedLocalidadComunitaria";
            ModifiedBy = _user;
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public class Builder
        {
            private readonly LocalidadComunitaria _LocalidadComunitaria = new LocalidadComunitaria();

            public Builder ConNombre(string nombre)
            {
                _LocalidadComunitaria.Nombre = nombre;
                return this;
            }

            public Builder ConUbicacion(string ubicacion)
            {
                _LocalidadComunitaria.Ubicacion = ubicacion;
                return this;
            }

            public Builder ConZona(int zonaId)
            {
                _LocalidadComunitaria.ZonaId = zonaId;
                return this;
            }


            public Builder WithAuditFields(string user)
            {
                _LocalidadComunitaria.CrudOperation = "Added";
                _LocalidadComunitaria.TransactionDate = DateTime.Now;
                _LocalidadComunitaria.TransactionType = "NuevaLocaidadComunitaria";
                _LocalidadComunitaria.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _LocalidadComunitaria.TransactionUId = Guid.NewGuid();
                _LocalidadComunitaria.Enabled = true;

                return this;
            }

            public LocalidadComunitaria Build()
            {
                return _LocalidadComunitaria;
            }
        }
    }
}