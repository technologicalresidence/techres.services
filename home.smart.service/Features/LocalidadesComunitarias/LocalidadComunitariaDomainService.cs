using System;

namespace home.smart.service.Features.LocalidadesComunitarias
{
    public class LocalidadComunitariaDomainService : ILocalidadComunitariaDomainService
    {
        public LocalidadComunitaria Create(LocalidadComunitariaRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Nombre)) throw new ArgumentNullException(nameof(request.Nombre));

            LocalidadComunitaria zona = new LocalidadComunitaria.Builder()
            .ConNombre(request.Nombre)
            .ConUbicacion(request.Ubicacion)
            .ConZona(request.ZonaId)
            .Build();

            return zona;
        }

        public LocalidadComunitaria Update(LocalidadComunitariaRequest request, LocalidadComunitaria _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(
                request.Nombre, request.Ubicacion, request.ZonaId, request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        }
    }
}