using home.smart.service.Core;

namespace home.smart.service.Features.LocalidadesComunitarias
{
    public class LocalidadComunitariaDTO : ResponseBase
    {
        public string Nombre { get; set; }
        public string Ubicacion { get; set; }
        public int ZonaId { get; set; }
    }
}