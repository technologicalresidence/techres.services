using System;
using home.smart.service.Core;
using Microsoft.AspNetCore.Mvc;

namespace home.smart.service.Features.LocalidadesComunitarias
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocalidadComunitariaController : ControllerBase
    {
        private readonly ILocalidadComunitariaAppService _localidadComunitariaAppService;
        public LocalidadComunitariaController(ILocalidadComunitariaAppService localidadComunitariaAppService)
        {
            if (localidadComunitariaAppService == null) throw new ArgumentException(nameof(localidadComunitariaAppService));

            _localidadComunitariaAppService = localidadComunitariaAppService;
        }
        [HttpGet]
        [Route("paged")]
        // [Authorize]
        public ActionResult<LocalidadComunitariaPgedDTO> GetPaged([FromQuery]PagedGeneralRequest request)
        {
            return Ok(_localidadComunitariaAppService.GetPaged(request));
        }

        [HttpPost]
        // [Authorize]
        public ActionResult<LocalidadComunitariaDTO> Post([FromBody]LocalidadComunitariaRequest request)
        {
            return Ok(_localidadComunitariaAppService.Create(request));
        }

        [HttpPut]
        // [Authorize]
        public ActionResult<LocalidadComunitariaDTO> Put([FromBody] LocalidadComunitariaRequest request)
        {
            return Ok(_localidadComunitariaAppService.Update(request));
        }

        [HttpDelete]
        // [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_localidadComunitariaAppService.Delete(Id));
        }
    }
}