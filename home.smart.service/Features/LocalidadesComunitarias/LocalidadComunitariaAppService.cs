using System;
using System.Linq;
using home.smart.service.Core;
using home.smart.service.DataContext;
using home.smart.service.Features.LocalidadesComunitarias;

namespace home.smart.service.Features.LocalidadesComunitariasComunitarias
{
    public class LocalidadComunitariaAppService : ILocalidadComunitariaAppService
    {
        private SmartHomeContext _context;
        private readonly ILocalidadComunitariaDomainService _localidadComunitariaDomainService;

        public LocalidadComunitariaAppService(SmartHomeContext context, ILocalidadComunitariaDomainService localidadComunitariaDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (localidadComunitariaDomainService == null) throw new ArgumentException(nameof(localidadComunitariaDomainService));

            _context = context;
            _localidadComunitariaDomainService = localidadComunitariaDomainService;
        }
        public LocalidadComunitariaDTO Create(LocalidadComunitariaRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newLocalidadComunitaria = _localidadComunitariaDomainService.Create(request);

            _context.LocalidadesComunitarias.Add(newLocalidadComunitaria);
            _context.SaveChanges();

            return new LocalidadComunitariaDTO
            {
                Nombre = newLocalidadComunitaria.Nombre,
                Ubicacion = newLocalidadComunitaria.Ubicacion,
                Enabled = newLocalidadComunitaria.Enabled
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var zona = _context.LocalidadesComunitarias.FirstOrDefault(s => s.Id == id);
            if (zona == null) throw new Exception("Error al intentar obtener país");
            _context.LocalidadesComunitarias.Remove(zona);
            _context.SaveChanges();

            return string.Empty;
        }

        public LocalidadComunitariaPgedDTO GetPaged(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.LocalidadesComunitarias.Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.LocalidadesComunitarias
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new LocalidadComunitariaPgedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new LocalidadComunitariaDTO
                    {
                        Id = s.Id,
                        Nombre = s.Nombre,
                        Ubicacion = s.Ubicacion,
                        ZonaId = s.ZonaId,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.LocalidadesComunitarias.Where(s => s.Nombre.Contains(request.Value)).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.LocalidadesComunitarias.Where(s => s.Nombre.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new LocalidadComunitariaPgedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = lista.Select(s => new LocalidadComunitariaDTO
                    {
                        Id = s.Id,
                        Nombre = s.Nombre,
                        Ubicacion = s.Ubicacion,
                        ZonaId = s.ZonaId,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
        }

        public LocalidadComunitariaDTO Update(LocalidadComunitariaRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldLocalidadComunitariaInfo = _context.LocalidadesComunitarias.FirstOrDefault(s => s.Id == request.Id);
            if (oldLocalidadComunitariaInfo == null) return new LocalidadComunitariaDTO { ValidationErrorMessage = "Error al obtener info de País" };

            var rolUpdate = _localidadComunitariaDomainService.Update(request, oldLocalidadComunitariaInfo);

            _context.LocalidadesComunitarias.Update(oldLocalidadComunitariaInfo);
            _context.SaveChanges();

            return new LocalidadComunitariaDTO
            {
                Id = oldLocalidadComunitariaInfo.Id,
                Nombre = oldLocalidadComunitariaInfo.Nombre,
                Ubicacion = oldLocalidadComunitariaInfo.Ubicacion,
                ZonaId = oldLocalidadComunitariaInfo.ZonaId,
                Enabled = oldLocalidadComunitariaInfo.Enabled
            };
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_localidadComunitariaDomainService != null) _localidadComunitariaDomainService.Dispose();
        }  
    }
}