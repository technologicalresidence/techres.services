using home.smart.service.Core;

namespace home.smart.service.Features.LocalidadesComunitarias
{
    public class LocalidadComunitariaRequest : RequestBase
    {
        public string Nombre { get; set; }
        public string Ubicacion { get; set; }
        public int ZonaId { get; set; }
    }
}