using System;

namespace home.smart.service.Features.LocalidadesComunitarias
{
    public interface ILocalidadComunitariaDomainService : IDisposable
    {
        LocalidadComunitaria Create(LocalidadComunitariaRequest request);
        LocalidadComunitaria Update(LocalidadComunitariaRequest request, LocalidadComunitaria _oldRegister);
    }
}