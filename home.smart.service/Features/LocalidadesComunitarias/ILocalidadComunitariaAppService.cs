using System;
using home.smart.service.Core;

namespace home.smart.service.Features.LocalidadesComunitarias
{
    public interface ILocalidadComunitariaAppService : IDisposable
    {
        LocalidadComunitariaPgedDTO GetPaged(PagedGeneralRequest request);
        LocalidadComunitariaDTO Create(LocalidadComunitariaRequest request);
        LocalidadComunitariaDTO Update(LocalidadComunitariaRequest request);
        string Delete(int id);
    }
}