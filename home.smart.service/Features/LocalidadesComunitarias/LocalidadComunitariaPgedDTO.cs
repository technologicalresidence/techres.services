using System.Collections.Generic;

namespace home.smart.service.Features.LocalidadesComunitarias
{
    public class LocalidadComunitariaPgedDTO
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<LocalidadComunitariaDTO> Items { get; set; }
    }
}