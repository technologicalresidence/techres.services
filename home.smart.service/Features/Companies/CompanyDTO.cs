using home.smart.service.Core;

namespace home.smart.service.Features.Companies
{
    public class CompanyDTO : ResponseBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Country { get; set; }
    }
}