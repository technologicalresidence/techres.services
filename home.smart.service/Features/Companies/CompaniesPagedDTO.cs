using System.Collections.Generic;
using home.smart.service.Core;

namespace home.smart.service.Features.Companies
{
    public class CompaniesPagedDTO : ResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<CompanyDTO> Items { get; set; }
    }
}