using System;
using home.smart.service.Core;

namespace home.smart.service.Features.Companies
{
    public interface ICompanyAppService : IDisposable
    {
        CompaniesPagedDTO GetPaged(PagedGeneralRequest request);
        CompanyDTO Create(CompanyRequest request);
        CompanyDTO Update(CompanyRequest request);
        string Delete(int id); 
    }
}