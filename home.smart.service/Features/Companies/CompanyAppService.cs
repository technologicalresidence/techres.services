using System;
using System.Linq;
using home.smart.service.Core;
using home.smart.service.DataContext;

namespace home.smart.service.Features.Companies
{
    public class CompanyAppService : ICompanyAppService
    {
        private SmartHomeContext _context;
        private readonly ICompanyDomainService _companyDomainService;

        public CompanyAppService(SmartHomeContext context, ICompanyDomainService companyDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (companyDomainService == null) throw new ArgumentException(nameof(companyDomainService));

            _context = context;
            _companyDomainService = companyDomainService;
        }
        public CompanyDTO Create(CompanyRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newCompany = _companyDomainService.Create(request);

            _context.Companies.Add(newCompany);
            _context.SaveChanges();

            return new CompanyDTO
            {
                Name = newCompany.Name,
                Description = newCompany.Description,
                Enabled = newCompany.Enabled
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var country = _context.Companies.FirstOrDefault(s => s.Id == id);
            if (country == null) throw new Exception("Error al intentar obtener país");
            _context.Companies.Remove(country);
            _context.SaveChanges();

            return string.Empty;
        }

        public CompaniesPagedDTO GetPaged(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Companies.Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Companies
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new CompaniesPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new CompanyDTO
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Description = s.Description,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.Companies.Where(s => s.Name.Contains(request.Value)).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Companies.Where(s => s.Name.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new CompaniesPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = lista.Select(s => new CompanyDTO
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Description = s.Description,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
        }

        public CompanyDTO Update(CompanyRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldCompanyInfo = _context.Companies.FirstOrDefault(s => s.Id == request.Id);
            if (oldCompanyInfo == null) return new CompanyDTO { ValidationErrorMessage = "Error al obtener info de País" };

            var rolUpdate = _companyDomainService.Update(request, oldCompanyInfo);

            _context.Companies.Update(oldCompanyInfo);
            _context.SaveChanges();

            return new CompanyDTO
            {
                Id = oldCompanyInfo.Id,
                Name = oldCompanyInfo.Name,
                Description = oldCompanyInfo.Description,
                Enabled = oldCompanyInfo.Enabled
            };
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_companyDomainService != null) _companyDomainService.Dispose();
        }
    }
}