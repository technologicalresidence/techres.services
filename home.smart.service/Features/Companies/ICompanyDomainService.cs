using System;

namespace home.smart.service.Features.Companies
{
    public interface ICompanyDomainService : IDisposable
    {
        Company Create(CompanyRequest request);
        Company Update(CompanyRequest request, Company _oldRegister);
    }
}