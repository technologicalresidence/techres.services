using System;

namespace home.smart.service.Features.Companies
{
    public class CompanyDomainService : ICompanyDomainService
    {
        public Company Create(CompanyRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Name)) throw new ArgumentNullException(nameof(request.Name));

            Company Company = new Company.Builder()
            .WithName(request.Name)
            .WithDescription(request.Description)
            .WithAuditFields(request.User)
            .WithCountry(request.Country)
            .Build();

            return Company;
        }

        public Company Update(CompanyRequest request, Company _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(request.Name, request.Description, request.User, request.Country);

            return _oldRegister;
        }

        public void Dispose()
        {
        }
    }
}