using System;
using home.smart.service.Core;
using Microsoft.AspNetCore.Mvc;

namespace home.smart.service.Features.Companies
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private readonly ICompanyAppService _companyAppService;
        public CompanyController(ICompanyAppService companyAppService)
        {
            if (companyAppService == null) throw new ArgumentException(nameof(companyAppService));

            _companyAppService = companyAppService;
        }
        [HttpGet]
        [Route("paged")]
        // [Authorize]
        public ActionResult<CompaniesPagedDTO> GetPaged([FromQuery]PagedGeneralRequest request)
        {
            return Ok(_companyAppService.GetPaged(request));
        }

        [HttpPost]
        // [Authorize]
        public ActionResult<CompanyDTO> Post([FromBody]CompanyRequest request)
        {
            return Ok(_companyAppService.Create(request));
        }

        [HttpPut]
        // [Authorize]
        public ActionResult<CompanyDTO> Put([FromBody] CompanyRequest request)
        {
            return Ok(_companyAppService.Update(request));
        }

        [HttpDelete]
        // [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_companyAppService.Delete(Id));
        }
    }
}