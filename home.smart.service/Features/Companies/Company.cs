using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using home.smart.service.Core;
using home.smart.service.Features.Countries;
using home.smart.service.Features.Users;
using home.smart.service.Features.Zones;

namespace home.smart.service.Features.Companies
{
    [Table("Companies")]
    public class Company : Entity
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public int Country { get; private set; }

        public Country Countries { get; set; }
        // public ICollection<User> Users { get; set; }
        public ICollection<Zona> Zonas { get; set; }

        public void Update(
            string _name, string _description, string user, int _country)
        {
            Name = _name;
            Description = _description;
            Country = _country;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedProject";
            ModifiedBy = user;
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public class Builder
        {
            private readonly Company _company = new Company();

            public Builder WithName(string name)
            {
                _company.Name = name;
                return this;
            }

            public Builder WithDescription(string description)
            {
                _company.Description = description;
                return this;
            }

            public Builder WithCountry(int country)
            {
                _company.Country = country;
                return this;
            }

            public Builder WithAuditFields(string user)
            {
                _company.CrudOperation = "Added";
                _company.TransactionDate = DateTime.Now;
                _company.TransactionType = "NewProject";
                _company.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" :  user;
                _company.TransactionUId = Guid.NewGuid();
                _company.Enabled = true;

                return this;
            }

            public Company Build()
            {
                return _company;
            }
        }
    }
}