using System;
using System.Linq;
using home.smart.service.Core;
using home.smart.service.DataContext;
using home.smart.service.Features.Zones;

namespace home.smart.service.Features.Zones
{
    public class ZonaAppService : IZonaAppService
    {
        private SmartHomeContext _context;
        private readonly IZonaDomainService _zonaDomainService;

        public ZonaAppService(SmartHomeContext context, IZonaDomainService zonaDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (zonaDomainService == null) throw new ArgumentException(nameof(zonaDomainService));

            _context = context;
            _zonaDomainService = zonaDomainService;
        }
        public ZonaDTO Create(ZonaRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newZona = _zonaDomainService.Create(request);

            _context.Zonas.Add(newZona);
            _context.SaveChanges();

            return new ZonaDTO
            {
                Nombre = newZona.Nombre,
                Descripcion = newZona.Descripcion,
                Enabled = newZona.Enabled
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var zona = _context.Zonas.FirstOrDefault(s => s.Id == id);
            if (zona == null) throw new Exception("Error al intentar obtener país");
            _context.Zonas.Remove(zona);
            _context.SaveChanges();

            return string.Empty;
        }

        public ZonaPagedDTO GetPaged(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Zonas.Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Zonas
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ZonaPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new ZonaDTO
                    {
                        Id = s.Id,
                        Nombre = s.Nombre,
                        Descripcion = s.Descripcion,
                        EmpresaId = s.EmpresaId,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.Zonas.Where(s => s.Nombre.Contains(request.Value)).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Zonas.Where(s => s.Nombre.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ZonaPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = lista.Select(s => new ZonaDTO
                    {
                        Id = s.Id,
                        Nombre = s.Nombre,
                        Descripcion = s.Descripcion,
                        EmpresaId = s.EmpresaId,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
        }

        public ZonaDTO Update(ZonaRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldzonaInfo = _context.Zonas.FirstOrDefault(s => s.Id == request.Id);
            if (oldzonaInfo == null) return new ZonaDTO { ValidationErrorMessage = "Error al obtener info de País" };

            var rolUpdate = _zonaDomainService.Update(request, oldzonaInfo);

            _context.Zonas.Update(oldzonaInfo);
            _context.SaveChanges();

            return new ZonaDTO
            {
                Id = oldzonaInfo.Id,
                Nombre = oldzonaInfo.Nombre,
                Descripcion = oldzonaInfo.Descripcion,
                Enabled = oldzonaInfo.Enabled
            };
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_zonaDomainService != null) _zonaDomainService.Dispose();
        }
        
    }
}