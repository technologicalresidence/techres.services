using System;

namespace home.smart.service.Features.Zones
{
    public class ZonaDomainService : IZonaDomainService
    {
        public Zona Create(ZonaRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Nombre)) throw new ArgumentNullException(nameof(request.Nombre));

            Zona zona = new Zona.Builder()
            .ConNombre(request.Nombre)
            .ConDescripcion(request.Descripcion)
            .ConDescripcion(request.User)
            .ConEmpresaId(request.EmpresaId)
            .Build();

            return zona;
        }

        public Zona Update(ZonaRequest request, Zona _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(
                request.Nombre, request.Descripcion, request.Direccion, request.EmpresaId ,request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        }
    }
}