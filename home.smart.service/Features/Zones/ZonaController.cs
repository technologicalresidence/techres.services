using System;
using home.smart.service.Core;
using Microsoft.AspNetCore.Mvc;

namespace home.smart.service.Features.Zones
{
    [Route("api/[controller]")]
    [ApiController]
    public class ZonaController : ControllerBase
    {
        private readonly IZonaAppService _zonaAppService;
        public ZonaController(IZonaAppService zonaAppService)
        {
            if (zonaAppService == null) throw new ArgumentException(nameof(zonaAppService));

            _zonaAppService = zonaAppService;
        }
        [HttpGet]
        [Route("paged")]
        // [Authorize]
        public ActionResult<ZonaPagedDTO> GetPaged([FromQuery]PagedGeneralRequest request)
        {
            return Ok(_zonaAppService.GetPaged(request));
        }

        [HttpPost]
        // [Authorize]
        public ActionResult<ZonaDTO> Post([FromBody]ZonaRequest request)
        {
            return Ok(_zonaAppService.Create(request));
        }

        [HttpPut]
        // [Authorize]
        public ActionResult<ZonaDTO> Put([FromBody] ZonaRequest request)
        {
            return Ok(_zonaAppService.Update(request));
        }

        [HttpDelete]
        // [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_zonaAppService.Delete(Id));
        }
    }
}