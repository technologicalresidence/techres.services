using System;
using home.smart.service.Core;

namespace home.smart.service.Features.Zones
{
    public interface IZonaAppService : IDisposable
    {
        ZonaPagedDTO GetPaged(PagedGeneralRequest request);
        ZonaDTO Create(ZonaRequest request);
        ZonaDTO Update(ZonaRequest request);
        string Delete(int id);
    }
}