using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using home.smart.service.Core;
using home.smart.service.Features.Companies;
using home.smart.service.Features.Localidades;
using home.smart.service.Features.LocalidadesComunitarias;

namespace home.smart.service.Features.Zones
{
    [Table("Zonas")]
    public class Zona : Entity
    {
        public string Nombre { get; private set; }
        public string Descripcion { get; private set; }
        public string Direccion { get; private set; }
        public int EmpresaId { get; private set; }

        public Company Emprea { get; set; }
        public ICollection<Localidad> Localidades { get; set; }
        public ICollection<LocalidadComunitaria> LocalidadesComunitarias { get; set; }


        public void Update(
           string _name, string _description, string _direccion, int empresaId,string user)
        {
            Nombre = _name;
            Descripcion = _description;
            Direccion = Direccion;
            EmpresaId = empresaId;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedProject";
            ModifiedBy = user;
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public class Builder
        {
            private readonly Zona _zona = new Zona();

            public Builder ConNombre(string nombre)
            {
                _zona.Nombre = nombre;
                return this;
            }

            public Builder ConDescripcion(string descripcion)
            {
                _zona.Descripcion = descripcion;
                return this;
            }

            public Builder ConDireccion(string direccion)
            {
                _zona.Direccion = direccion;
                return this;
            }

            public Builder ConEmpresaId(int empresaId)
            {
                _zona.EmpresaId = empresaId;
                return this;
            }

            public Builder WithAuditFields(string user)
            {
                _zona.CrudOperation = "Added";
                _zona.TransactionDate = DateTime.Now;
                _zona.TransactionType = "CrearZona";
                _zona.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _zona.TransactionUId = Guid.NewGuid();
                _zona.Enabled = true;

                return this;
            }

            public Zona Build()
            {
                return _zona;
            }
        }

    }
}