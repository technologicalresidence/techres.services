using System;

namespace home.smart.service.Features.Zones
{
    public interface IZonaDomainService : IDisposable
    {
        Zona Create(ZonaRequest request);
        Zona Update(ZonaRequest request, Zona _oldRegister);   
    }
}