using System.Collections.Generic;
using home.smart.service.Core;

namespace home.smart.service.Features.Zones
{
    public class ZonaPagedDTO : ResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<ZonaDTO> Items { get; set; }
    }
}