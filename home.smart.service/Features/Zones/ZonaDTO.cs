using home.smart.service.Core;

namespace home.smart.service.Features.Zones
{
    public class ZonaDTO : ResponseBase
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Direccion { get; set; }
        public int EmpresaId { get; set; }
    }
}