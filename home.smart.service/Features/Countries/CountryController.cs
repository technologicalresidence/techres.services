using System;
using home.smart.service.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace home.smart.service.Features.Countries
{
    [Route("api/[controller]")]
    [ApiController]
    // [Authorize]
    public class CountryController : ControllerBase
    {
        private readonly ICountryAppService _countryAppService;
        public CountryController(ICountryAppService countryAppService)
        {
            if (countryAppService == null) throw new ArgumentException(nameof(countryAppService));

            _countryAppService = countryAppService;
        }
        [HttpGet]
        [Route("paged")]
        // [Authorize]
        public ActionResult<CountriesPagedDto> GetPaged([FromQuery]PagedGeneralRequest request)
        {
            return Ok(_countryAppService.GetPaged(request));
        }

        [HttpPost]
        // [Authorize]
        public ActionResult<CountryDTO> Post([FromBody]CountryRequest request)
        {
            return Ok(_countryAppService.Create(request));
        }

        [HttpPut]
        // [Authorize]
        public ActionResult<CountryDTO> Put([FromBody] CountryRequest request)
        {
            return Ok(_countryAppService.Update(request));
        }

        [HttpDelete]
        // [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_countryAppService.Delete(Id));
        }
    }
}