using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using home.smart.service.Core;
using home.smart.service.Features.Companies;

namespace home.smart.service.Features.Countries
{
    [Table("Countries")]
    public class Country : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<Company> Companies { get; set; }

        public void Update(
            string _name, string _description, string _user)
        {
            Name = _name;
            Description = _description;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedProject";
            ModifiedBy = _user;
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public class Builder
        {
            private readonly Country _country = new Country();

            public Builder WithName(string name)
            {
                _country.Name = name;
                return this;
            }

            public Builder WithDescription(string description)
            {
                _country.Description = description;
                return this;
            }

            public Builder WithAuditFields(string user)
            {
                _country.CrudOperation = "Added";
                _country.TransactionDate = DateTime.Now;
                _country.TransactionType = "NewProject";
                _country.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _country.TransactionUId = Guid.NewGuid();
                _country.Enabled = true;

                return this;
            }

            public Country Build()
            {
                return _country;
            }
        }
    }
}