using System.Collections.Generic;
using home.smart.service.Core;

namespace home.smart.service.Features.Countries
{
    public class CountriesPagedDto : ResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<CountryDTO> Items { get; set; }
    }
}