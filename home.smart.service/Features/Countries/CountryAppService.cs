using System;
using System.Linq;
using home.smart.service.Core;
using home.smart.service.DataContext;

namespace home.smart.service.Features.Countries
{
    public class CountryAppService : ICountryAppService
    {
        private SmartHomeContext _context;
        private readonly ICountryDomainService _countryDomainService;

        public CountryAppService(SmartHomeContext context, ICountryDomainService countryDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (countryDomainService == null) throw new ArgumentException(nameof(countryDomainService));

            _context = context;
            _countryDomainService = countryDomainService;
        }
        public CountryDTO Create(CountryRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newCountry = _countryDomainService.Create(request);

            _context.Countries.Add(newCountry);
            _context.SaveChanges();

            return new CountryDTO
            {
                Name = newCountry.Name,
                Description = newCountry.Description,
                Enabled = newCountry.Enabled
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var country = _context.Countries.FirstOrDefault(s => s.Id == id);
            if (country == null) throw new Exception("Error al intentar obtener país");
            _context.Countries.Remove(country);
            _context.SaveChanges();

            return string.Empty;
        }

        public CountriesPagedDto GetPaged(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;
            
            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Countries.Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Countries
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new CountriesPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new CountryDTO
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Description = s.Description,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.Countries.Where(s => s.Name.Contains(request.Value)).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Countries.Where(s => s.Name.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new CountriesPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = lista.Select(s => new CountryDTO
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Description = s.Description,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
        }

        public CountryDTO Update(CountryRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldCountryInfo = _context.Countries.FirstOrDefault(s => s.Id == request.Id);
            if (oldCountryInfo == null) return new CountryDTO { ValidationErrorMessage = "Error al obtener info de País" };

            var rolUpdate = _countryDomainService.Update(request, oldCountryInfo);

            _context.Countries.Update(oldCountryInfo);
            _context.SaveChanges();

            return new CountryDTO
            {
                Id = oldCountryInfo.Id,
                Name = oldCountryInfo.Name,
                Description = oldCountryInfo.Description,
                Enabled = oldCountryInfo.Enabled
            };
        }

        public void Dispose()
        {
           if (_context != null) _context.Dispose();
           if(_countryDomainService != null) _countryDomainService.Dispose();
        }
    }
}