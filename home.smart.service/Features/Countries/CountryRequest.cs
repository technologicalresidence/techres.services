using home.smart.service.Core;

namespace home.smart.service.Features.Countries
{
    public class CountryRequest : RequestBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}