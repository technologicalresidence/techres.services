using System;

namespace home.smart.service.Features.Countries
{
    public interface ICountryDomainService : IDisposable
    {
        Country Create(CountryRequest request);
        Country Update(CountryRequest request, Country _oldRegister);
    }
}