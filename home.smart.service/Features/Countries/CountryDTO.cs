using home.smart.service.Core;

namespace home.smart.service.Features.Countries
{
    public class CountryDTO : ResponseBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}