using System;

namespace home.smart.service.Features.Countries
{
    public class CountryDomainService : ICountryDomainService
    {
        public Country Create(CountryRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Name)) throw new ArgumentNullException(nameof(request.Name));

            Country country = new Country.Builder()
            .WithName(request.Name)
            .WithDescription(request.Description)
            .WithAuditFields(request.User)
            .Build();

            return country;
        }

        public Country Update(CountryRequest request, Country _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(
                request.Name, request.Description, request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        }
    }
}