using System;
using home.smart.service.Core;

namespace home.smart.service.Features.Countries
{
    public interface ICountryAppService : IDisposable
    {
        CountriesPagedDto GetPaged(PagedGeneralRequest request);
        CountryDTO Create(CountryRequest request);
        CountryDTO Update(CountryRequest request);
        string Delete(int id);
    }
}