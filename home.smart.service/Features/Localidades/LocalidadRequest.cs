using home.smart.service.Core;

namespace home.smart.service.Features.Localidades
{
    public class LocalidadRequest : RequestBase
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Direccion { get; set; }
        public int ZonaId { get; set; }
    }
}