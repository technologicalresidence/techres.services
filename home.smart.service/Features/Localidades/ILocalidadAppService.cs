using System;
using home.smart.service.Core;

namespace home.smart.service.Features.Localidades
{
    public interface ILocalidadAppService : IDisposable
    {
        LocalidadPagedDTO GetPaged(PagedGeneralRequest request);
        LocalidadDTO Create(LocalidadRequest request);
        LocalidadDTO Update(LocalidadRequest request);
        string Delete(int id);
    }
}