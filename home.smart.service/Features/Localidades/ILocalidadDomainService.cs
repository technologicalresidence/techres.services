using System;

namespace home.smart.service.Features.Localidades
{
    public interface ILocalidadDomainService : IDisposable
    {
        Localidad Create(LocalidadRequest request);
        Localidad Update(LocalidadRequest request, Localidad _oldRegister);
    }
}