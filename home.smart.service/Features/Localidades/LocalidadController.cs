using System;
using home.smart.service.Core;
using Microsoft.AspNetCore.Mvc;

namespace home.smart.service.Features.Localidades
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocalidadController : ControllerBase
    {
        private readonly ILocalidadAppService _localidadAppService;
        public LocalidadController(ILocalidadAppService localidadAppService)
        {
            if (localidadAppService == null) throw new ArgumentException(nameof(localidadAppService));

            _localidadAppService = localidadAppService;
        }
        [HttpGet]
        [Route("paged")]
        // [Authorize]
        public ActionResult<LocalidadPagedDTO> GetPaged([FromQuery]PagedGeneralRequest request)
        {
            return Ok(_localidadAppService.GetPaged(request));
        }

        [HttpPost]
        // [Authorize]
        public ActionResult<LocalidadDTO> Post([FromBody]LocalidadRequest request)
        {
            return Ok(_localidadAppService.Create(request));
        }

        [HttpPut]
        // [Authorize]
        public ActionResult<LocalidadDTO> Put([FromBody] LocalidadRequest request)
        {
            return Ok(_localidadAppService.Update(request));
        }

        [HttpDelete]
        // [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_localidadAppService.Delete(Id));
        } 
    }
}