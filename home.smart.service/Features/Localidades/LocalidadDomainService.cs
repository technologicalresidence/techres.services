using System;

namespace home.smart.service.Features.Localidades
{
    public class LocalidadDomainService : ILocalidadDomainService
    {
        public Localidad Create(LocalidadRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Nombre)) throw new ArgumentNullException(nameof(request.Nombre));

            Localidad zona = new Localidad.Builder()
            .ConNombre(request.Nombre)
            .ConDescripcion(request.Descripcion)
            .ConDescripcion(request.User)
            .ConEmpresaId(request.ZonaId)
            .Build();

            return zona;
        }

        public Localidad Update(LocalidadRequest request, Localidad _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(
                request.Nombre, request.Descripcion, request.Direccion, request.ZonaId, request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        }
    }
}