using System;
using System.Linq;
using home.smart.service.Core;
using home.smart.service.DataContext;

namespace home.smart.service.Features.Localidades
{
    public class LocalidadAppService : ILocalidadAppService
    {
        private SmartHomeContext _context;
        private readonly ILocalidadDomainService _localidadDomainService;

        public LocalidadAppService(SmartHomeContext context, ILocalidadDomainService localidadDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (localidadDomainService == null) throw new ArgumentException(nameof(localidadDomainService));

            _context = context;
            _localidadDomainService = localidadDomainService;
        }
        public LocalidadDTO Create(LocalidadRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newZona = _localidadDomainService.Create(request);

            _context.Localidades.Add(newZona);
            _context.SaveChanges();

            return new LocalidadDTO
            {
                Nombre = newZona.Nombre,
                Descripcion = newZona.Descripcion,
                Enabled = newZona.Enabled
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var zona = _context.Localidades.FirstOrDefault(s => s.Id == id);
            if (zona == null) throw new Exception("Error al intentar obtener país");
            _context.Localidades.Remove(zona);
            _context.SaveChanges();

            return string.Empty;
        }

        public LocalidadPagedDTO GetPaged(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Localidades.Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Localidades
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new LocalidadPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new LocalidadDTO
                    {
                        Id = s.Id,
                        Nombre = s.Nombre,
                        Descripcion = s.Descripcion,
                        ZonaId = s.ZonaId,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.Localidades.Where(s => s.Nombre.Contains(request.Value)).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Localidades.Where(s => s.Nombre.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new LocalidadPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = lista.Select(s => new LocalidadDTO
                    {
                        Id = s.Id,
                        Nombre = s.Nombre,
                        Descripcion = s.Descripcion,
                        ZonaId = s.ZonaId,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
        }

        public LocalidadDTO Update(LocalidadRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldzonaInfo = _context.Localidades.FirstOrDefault(s => s.Id == request.Id);
            if (oldzonaInfo == null) return new LocalidadDTO { ValidationErrorMessage = "Error al obtener info de País" };

            var rolUpdate = _localidadDomainService.Update(request, oldzonaInfo);

            _context.Localidades.Update(oldzonaInfo);
            _context.SaveChanges();

            return new LocalidadDTO
            {
                Id = oldzonaInfo.Id,
                Nombre = oldzonaInfo.Nombre,
                Descripcion = oldzonaInfo.Descripcion,
                Enabled = oldzonaInfo.Enabled
            };
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_localidadDomainService != null) _localidadDomainService.Dispose();
        }
    }
}