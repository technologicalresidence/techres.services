using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using home.smart.service.Core;
using home.smart.service.Features.Residentes;
using home.smart.service.Features.Zones;

namespace home.smart.service.Features.Localidades
{
    [Table("Localidades")]
    public class Localidad : Entity
    {
        public string Nombre { get; private set; }
        public string Descripcion { get; private set; }
        public string Direccion { get; private set; }
        public int ZonaId { get; private set; }

        public Zona Zona { get; set; }
        public ICollection<Residente> Residentes { get; set; }

        public void Update(
       string _name, string _description, string _direccion, int zonaId, string user)
        {
            Nombre = _name;
            Descripcion = _description;
            Direccion = _direccion;
            ZonaId = zonaId;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModificarLocalidad";
            ModifiedBy = user;
            TransactionUId = Guid.NewGuid();
            Enabled = true;
        }

        public class Builder
        {
            private readonly Localidad _localidad = new Localidad();

            public Builder ConNombre(string nombre)
            {
                _localidad.Nombre = nombre;
                return this;
            }

            public Builder ConDescripcion(string descripcion)
            {
                _localidad.Descripcion = descripcion;
                return this;
            }

            public Builder ConDireccion(string direccion)
            {
                _localidad.Direccion = direccion;
                return this;
            }

            public Builder ConEmpresaId(int zonaId)
            {
                _localidad.ZonaId = zonaId;
                return this;
            }

            public Builder WithAuditFields(string user)
            {
                _localidad.CrudOperation = "Added";
                _localidad.TransactionDate = DateTime.Now;
                _localidad.TransactionType = "CrearLocalidad";
                _localidad.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _localidad.TransactionUId = Guid.NewGuid();
                _localidad.Enabled = true;

                return this;
            }

            public Localidad Build()
            {
                return _localidad;
            }
        }
    }
}