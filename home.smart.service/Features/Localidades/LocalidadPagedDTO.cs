using System.Collections.Generic;
using home.smart.service.Core;

namespace home.smart.service.Features.Localidades
{
    public class LocalidadPagedDTO : ResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<LocalidadDTO> Items { get; set; }
    }
}