using System;
using home.smart.service.Core;
using Microsoft.AspNetCore.Mvc;

namespace home.smart.service.Features.Users
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly IUserAppService _usuarioAppService;
        public UsuarioController(IUserAppService usuarioAppService)
        {
            if (usuarioAppService == null) throw new ArgumentException(nameof(usuarioAppService));

            _usuarioAppService = usuarioAppService;
        }
        [HttpGet]
        [Route("paged")]
        // [Authorize]
        public ActionResult<UserPagedDTO> GetPaged([FromQuery]PagedGeneralRequest request)
        {
            return Ok(_usuarioAppService.GetPagedUser(request));
        }

        [HttpPost]
        // [Authorize]
        public ActionResult<UserDTO> Post([FromBody]UserRequest request)
        {
            return Ok(_usuarioAppService.CreateUser(request));
        }

        [HttpPut]
        // [Authorize]
        public ActionResult<UserDTO> Put([FromBody] UserRequest request)
        {
            return Ok(_usuarioAppService.UpdateUser(request));
        }

        [HttpPut]
        [Route("disabled")]
        public ActionResult<UserDTO> Disabled([FromBody] UserRequest request)
        {
            return Ok(_usuarioAppService.Disabled(request));
        }
    }
}