using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using home.smart.service.Core;
using home.smart.service.Features.Companies;

namespace home.smart.service.Features.Users
{
    [Table("Users")]
    public class User : Entity
    {
        public string UserId { get; private set; }
        public string FirstName { get; private set; }
        public string SecondName { get; private set; }
        public string LastName { get; private set; }
        public string email { get; private set; }
        public string PhoneNumber { get; private set; }
        public int CompanyId { get; set; }
        public int ClienteId { get; set; }
        public bool Status { get; set; }

        public ICollection<UserPassword> Passwords { get; set; }

        public void DisbaledUser(string user)
        {
            CrudOperation = "Disabled";
            TransactionDate = DateTime.Now;
            TransactionType = "DisabledUser";
            ModifiedBy = string.IsNullOrEmpty(user) ? "Service" : user;
            TransactionUId = Guid.NewGuid();
            Status = false;
        }
        public void Update(
             string firstName,
             string secondName,
             string lastName,
             string _email,
             string phoneNumber,
             int clienteId)
        {
            FirstName = firstName;
            SecondName = secondName;
            LastName = lastName;
            email = _email;
            PhoneNumber = phoneNumber;
            ClienteId = clienteId;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedUser";
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
            Status = true;
        }

        public class Builder
        {
            private readonly User _user = new User();

            public Builder WithUserId(string userid)
            {
                _user.UserId = userid;
                return this;
            }
            public Builder WithFirstName(string firstName)
            {
                _user.FirstName = firstName;
                return this;
            }
            public Builder WithSecondName(string secondName)
            {
                _user.SecondName = secondName;
                return this;
            }
            public Builder WithLastName(string lastName)
            {
                _user.LastName = lastName;
                return this;
            }

            public Builder WithEmail(string email)
            {
                _user.email = email;
                return this;
            }
            public Builder WithPhoneNumber(string phone)
            {
                _user.PhoneNumber = phone;
                return this;
            }

            public Builder WithCliente(int clienteId)
            {
                _user.ClienteId = clienteId;
                return this;
            }

            public Builder WithAuditFields(string user)
            {
                _user.CrudOperation = "Added";
                _user.TransactionDate = DateTime.Now;
                _user.TransactionType = "NewProject";
                _user.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _user.TransactionUId = Guid.NewGuid();
                _user.Enabled = true;

                return this;
            }

            public User Build()
            {
                return _user;
            }
        }
    }
}