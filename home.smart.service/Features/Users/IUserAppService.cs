using System;
using home.smart.service.Core;

namespace home.smart.service.Features.Users
{
    public interface IUserAppService : IDisposable
    {
        UserPagedDTO GetPagedUser(PagedGeneralRequest request);
        UserDTO CreateUser(UserRequest request);
        UserDTO UpdateUser(UserRequest request);
        UserDTO Disabled(UserRequest request);
        string DeteleUser(string userId);
    }
}