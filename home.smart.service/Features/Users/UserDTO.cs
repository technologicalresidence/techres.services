using home.smart.service.Core;

namespace home.smart.service.Features.Users
{
    public class UserDTO : ResponseBase
    {
        public string UserId { get;  set; }
        public string FirstName { get;  set; }
        public string SecondName { get;  set; }
        public string LastName { get;  set; }
        public string email { get;  set; }
        public string PhoneNumber { get;  set; }
        public int CompanyId { get; set; }
        public int ClienteId { get; set;}
    }
}