using System;
using System.ComponentModel.DataAnnotations.Schema;
using home.smart.service.Core;

namespace home.smart.service.Features.Users
{
    [Table("UserPasswords")]
    public class UserPassword : Entity
    {
        public int UserId { get; private set; }
        public string PasswordHash { get; private set; }
        public string Password { get; private set; }
        public User User { get; set; }

        public void DisablePassword(
                     string newPassword, string user)
        {
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ChangePassword";
            ModifiedBy = user;
            TransactionUId = Guid.NewGuid();
            Enabled = false;
        }

        public class Builder
        {
            private readonly UserPassword _userPassword = new UserPassword();

            public Builder WithPasswordHash(string passwordHash)
            {
                _userPassword.PasswordHash = passwordHash;
                return this;
            }
            public Builder WithPassword(int userId, string password)
            {
                _userPassword.Password = password;
                _userPassword.PasswordHash = password;
                _userPassword.UserId = userId;
                return this;
            }

            public Builder WithAuditFields(string user)
            {
                _userPassword.CrudOperation = "Added";
                _userPassword.TransactionDate = DateTime.Now;
                _userPassword.TransactionType = "NewProject";
                _userPassword.ModifiedBy = string.IsNullOrEmpty(user) ? "Aplication" : user;
                _userPassword.TransactionUId = Guid.NewGuid();
                _userPassword.Enabled = true;

                return this;
            }

            public UserPassword Build()
            {
                return _userPassword;
            }
        }

    }
}