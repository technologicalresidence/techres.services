using System;

namespace home.smart.service.Features.Users
{
    public interface IUserDomainService : IDisposable
    {
        User Create(UserRequest request);
        User Update(UserRequest request, User _oldRegister);
        User DisabledUser(UserRequest request, User _oldRegister);
        UserPassword CreatePassword(UserRequest request,string password, int usuarioId);
    }
}