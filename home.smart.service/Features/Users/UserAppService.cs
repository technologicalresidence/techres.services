using System;
using System.Linq;
using home.smart.service.Core;
using home.smart.service.DataContext;

namespace home.smart.service.Features.Users
{
    public class UserAppService : IUserAppService
    {
        private SmartHomeContext _context;
        private readonly IUserDomainService _userDomainService;

        public UserAppService(SmartHomeContext context,
                                   IUserDomainService userDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (userDomainService == null) throw new ArgumentException(nameof(userDomainService));

            _context = context;
            _userDomainService = userDomainService;
        }
        public UserDTO CreateUser(UserRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newUsuario = _userDomainService.Create(request);

            _context.Usuarios.Add(newUsuario);
            _context.SaveChanges();

            var usuario = _context.Usuarios.FirstOrDefault(s => s.UserId == request.UserId);
            var newPassword = _userDomainService.CreatePassword(request, request.Password, usuario.Id);
            _context.Passwords.Add(newPassword);
            _context.SaveChanges();

            return new UserDTO
            {
                FirstName = newUsuario.FirstName,
                SecondName = newUsuario.SecondName,
                LastName = newUsuario.LastName,
                email = newUsuario.email,
                PhoneNumber = newUsuario.PhoneNumber,
                CompanyId = newUsuario.CompanyId,
                Enabled = newUsuario.Enabled
            };
        }

        public string DeteleUser(string userId)
        {
            throw new System.NotImplementedException();
        }

        public UserDTO Disabled(UserRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldUsuarioInfo = _context.Usuarios.FirstOrDefault(s => s.Id == request.Id);
            if (oldUsuarioInfo == null) return new UserDTO { ValidationErrorMessage = "Error al obtener info de usuario" };

            var userUpdate = _userDomainService.DisabledUser(request, oldUsuarioInfo);

            _context.Usuarios.Update(oldUsuarioInfo);
            _context.SaveChanges();

            return new UserDTO
            {
                Id = oldUsuarioInfo.Id,
                FirstName = oldUsuarioInfo.FirstName,
                SecondName = oldUsuarioInfo.SecondName,
                LastName = oldUsuarioInfo.LastName,
                email = oldUsuarioInfo.email,
                PhoneNumber = oldUsuarioInfo.PhoneNumber,
                CompanyId = oldUsuarioInfo.CompanyId,
                Enabled = oldUsuarioInfo.Enabled
            };
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_userDomainService != null) _userDomainService.Dispose();
        }

        public UserPagedDTO GetPagedUser(PagedGeneralRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Usuarios.Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Usuarios
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new UserPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = lista.Select(s => new UserDTO
                    {
                        Id = s.Id,
                        FirstName = s.FirstName,
                        SecondName = s.SecondName,
                        LastName = s.LastName,
                        email = s.email,
                        PhoneNumber = s.PhoneNumber,
                        CompanyId = s.CompanyId,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
            else
            {

                var count = Convert.ToDecimal(_context.Usuarios.Where(s => s.UserId.Contains(request.Value)).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.Usuarios.Where(s => s.UserId.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new UserPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = lista.Select(s => new UserDTO
                    {
                        Id = s.Id,
                        FirstName = s.FirstName,
                        SecondName = s.SecondName,
                        LastName = s.LastName,
                        email = s.email,
                        PhoneNumber = s.PhoneNumber,
                        CompanyId = s.CompanyId,
                        Enabled = s.Enabled
                    }).ToList()
                };
            }
        }

        public UserDTO UpdateUser(UserRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldUsuarioInfo = _context.Usuarios.FirstOrDefault(s => s.Id == request.Id);
            if (oldUsuarioInfo == null) return new UserDTO { ValidationErrorMessage = "Error al obtener info de usuario" };

            var userUpdate = _userDomainService.Update(request, oldUsuarioInfo);

            _context.Usuarios.Update(oldUsuarioInfo);
            _context.SaveChanges();

            return new UserDTO
            {
                Id = oldUsuarioInfo.Id,
                FirstName = oldUsuarioInfo.FirstName,
                SecondName = oldUsuarioInfo.SecondName,
                LastName = oldUsuarioInfo.LastName,
                email = oldUsuarioInfo.email,
                PhoneNumber = oldUsuarioInfo.PhoneNumber,
                CompanyId = oldUsuarioInfo.CompanyId,
                Enabled = oldUsuarioInfo.Enabled
            };
        }
    }
}