using System;
using home.smart.service.Core;

namespace home.smart.service.Features.Users
{
    public class UserDomainService : IUserDomainService
    {

        private EncriptorHelper _encriptorHelper = new EncriptorHelper();
        public User DisabledUser(UserRequest request, User _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.DisbaledUser(request.User);

            return _oldRegister;
        }

        public User Create(UserRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (string.IsNullOrEmpty(request.UserId)) throw new ArgumentException(nameof(request.UserId));
            if (string.IsNullOrEmpty(request.FirstName)) throw new ArgumentException(nameof(request.FirstName));
            if (String.IsNullOrEmpty(request.Password)) throw new ArgumentException(nameof(request.Password));

            return new User.Builder()
            .WithUserId(request.UserId)
            .WithFirstName(request.FirstName)
            .WithSecondName(request.SecondName ?? string.Empty)
            .WithLastName(request.LastName ?? string.Empty)
            .WithPhoneNumber(request.PhoneNumber ?? string.Empty)
            .WithEmail(request.email ?? string.Empty)
            .WithAuditFields(request.User)
            .Build();
        }

        public User Update(UserRequest request, User _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(request.FirstName, request.SecondName,request.LastName, request.email, request.PhoneNumber, request.ClienteId);

            return _oldRegister;
        }

        public void Dispose()
        {
        }

        public UserPassword CreatePassword(UserRequest request, string password, int usuarioId)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if(usuarioId <= 0) throw new ArgumentException(nameof(request));
            if (String.IsNullOrEmpty(request.Password)) throw new ArgumentException(nameof(request.Password));

            return new UserPassword.Builder()
            .WithPassword(usuarioId, _encriptorHelper.GetPasswordHash(password))
            .WithPasswordHash(_encriptorHelper.GetPasswordHash(password))
            .WithAuditFields(request.User)
            .Build();
        }
    }
}