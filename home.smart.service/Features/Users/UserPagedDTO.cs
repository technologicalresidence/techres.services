using System.Collections.Generic;
using home.smart.service.Core;

namespace home.smart.service.Features.Users
{
    public class UserPagedDTO : ResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<UserDTO> Items { get; set; }
    }
}