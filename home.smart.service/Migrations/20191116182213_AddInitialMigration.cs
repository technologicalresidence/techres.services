﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace home.smart.service.Migrations
{
    public partial class AddInitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Enabled = table.Column<bool>(nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Description = table.Column<string>(unicode: false, maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Enabled = table.Column<bool>(nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false),
                    UserId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    FirstName = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    SecondName = table.Column<string>(maxLength: 50, nullable: true),
                    LastName = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Email = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    PhoneNumber = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CompanyId = table.Column<int>(nullable: false),
                    ClienteId = table.Column<int>(nullable: false),
                    Status = table.Column<bool>(unicode: false, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Enabled = table.Column<bool>(nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Description = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    Country = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Companies_Countries_Country",
                        column: x => x.Country,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserPasswords",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Enabled = table.Column<bool>(nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false),
                    UserId = table.Column<int>(unicode: false, maxLength: 50, nullable: false),
                    PasswordHash = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Password = table.Column<string>(unicode: false, maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPasswords", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserPasswords_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Zonas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Enabled = table.Column<bool>(nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false),
                    Nombre = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Descripcion = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    Direccion = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    EmpresaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Zonas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Zonas_Companies_EmpresaId",
                        column: x => x.EmpresaId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Localidades",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Enabled = table.Column<bool>(nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false),
                    Nombre = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Descripcion = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    Direccion = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    ZonaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Localidades", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Localidades_Zonas_ZonaId",
                        column: x => x.ZonaId,
                        principalTable: "Zonas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LocalidadesComunitarias",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Enabled = table.Column<bool>(nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false),
                    Nombre = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Ubicacion = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    ZonaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocalidadesComunitarias", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LocalidadesComunitarias_Zonas_ZonaId",
                        column: x => x.ZonaId,
                        principalTable: "Zonas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Residentes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Enabled = table.Column<bool>(nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false),
                    Nombres = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    Appellido = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    Telefono = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    Identidad = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    Correo = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    LocalidadId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Residentes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Residentes_Localidades_LocalidadId",
                        column: x => x.LocalidadId,
                        principalTable: "Localidades",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Reservas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Enabled = table.Column<bool>(nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false),
                    CodigoReserva = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    ResidenteId = table.Column<int>(nullable: false),
                    VigenciaMinutos = table.Column<int>(nullable: false),
                    EstadoVigencia = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    LocalidadComunitariaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reservas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reservas_LocalidadesComunitarias_LocalidadComunitariaId",
                        column: x => x.LocalidadComunitariaId,
                        principalTable: "LocalidadesComunitarias",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Invitaciones",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Enabled = table.Column<bool>(nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false),
                    CodigoInvitacion = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    Invitado = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    NumeroTelefono = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    IdentidadDelInvitado = table.Column<string>(nullable: true),
                    ResidenteId = table.Column<int>(nullable: false),
                    VigenciaMinutos = table.Column<int>(nullable: false),
                    EstadoDeVigencia = table.Column<string>(unicode: false, maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invitaciones", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Invitaciones_Residentes_ResidenteId",
                        column: x => x.ResidenteId,
                        principalTable: "Residentes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Companies_Country",
                table: "Companies",
                column: "Country");

            migrationBuilder.CreateIndex(
                name: "IX_Invitaciones_ResidenteId",
                table: "Invitaciones",
                column: "ResidenteId");

            migrationBuilder.CreateIndex(
                name: "IX_Localidades_ZonaId",
                table: "Localidades",
                column: "ZonaId");

            migrationBuilder.CreateIndex(
                name: "IX_LocalidadesComunitarias_ZonaId",
                table: "LocalidadesComunitarias",
                column: "ZonaId");

            migrationBuilder.CreateIndex(
                name: "IX_Reservas_LocalidadComunitariaId",
                table: "Reservas",
                column: "LocalidadComunitariaId");

            migrationBuilder.CreateIndex(
                name: "IX_Residentes_LocalidadId",
                table: "Residentes",
                column: "LocalidadId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPasswords_UserId",
                table: "UserPasswords",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Zonas_EmpresaId",
                table: "Zonas",
                column: "EmpresaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Invitaciones");

            migrationBuilder.DropTable(
                name: "Reservas");

            migrationBuilder.DropTable(
                name: "UserPasswords");

            migrationBuilder.DropTable(
                name: "Residentes");

            migrationBuilder.DropTable(
                name: "LocalidadesComunitarias");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Localidades");

            migrationBuilder.DropTable(
                name: "Zonas");

            migrationBuilder.DropTable(
                name: "Companies");

            migrationBuilder.DropTable(
                name: "Countries");
        }
    }
}
