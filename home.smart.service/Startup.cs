﻿using System;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using home.smart.service.DataContext;
using Swashbuckle.AspNetCore.Swagger;
using home.smart.service.Features.Countries;
using home.smart.service.Features.Companies;
using home.smart.service.Features.Invitaciones;
using home.smart.service.Features.Localidades;
using home.smart.service.Features.LocalidadesComunitarias;
using home.smart.service.Features.LocalidadesComunitariasComunitarias;
using home.smart.service.Features.Reservas;
using home.smart.service.Features.Residentes;
using home.smart.service.Features.Users;
using home.smart.service.Features.Zones;

namespace home.smart.service
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SmartHomeContext>(
              options => options.UseMySql(
                Configuration.GetConnectionString(
                  "DefaultConnection")));


            // Add service and create Policy with options
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            services.AddMvc();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);


            services.AddTransient<SmartHomeContext>();
            services.AddScoped<ICountryDomainService, CountryDomainService>();
            services.AddScoped<ICountryAppService, CountryAppService>();
            services.AddScoped<ICompanyDomainService, CompanyDomainService>();
            services.AddScoped<ICompanyAppService, CompanyAppService>();
            services.AddScoped<IInvitacionDomainService, InvitacionDomainService>();
            services.AddScoped<IInvitacionAppService, InvitacionAppService>();
            services.AddScoped<ILocalidadDomainService, LocalidadDomainService>();
            services.AddScoped<ILocalidadAppService, LocalidadAppService>();
            services.AddScoped<ILocalidadComunitariaDomainService, LocalidadComunitariaDomainService>();
            services.AddScoped<ILocalidadComunitariaAppService, LocalidadComunitariaAppService>();
            services.AddScoped<IReservaDomainService, ReservaDomainService>();
            services.AddScoped<IReservaAppService, ReservaAppService>();
            services.AddScoped<IResidenteDomainService, ResidenteDomainService>();
            services.AddScoped<IResidenteAppService, ResidenteAppService>();
            services.AddScoped<IUserDomainService, UserDomainService>();
            services.AddScoped<IUserAppService, UserAppService>();
            services.AddScoped<IZonaDomainService, ZonaDomainService>();
            services.AddScoped<IZonaAppService, ZonaAppService>();

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Smart Home Services API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseAuthentication();

            app.UseCors("CorsPolicy");

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Smart Home Services API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseMvc();
        }
    }
}
