using home.smart.service.Core;
using home.smart.service.Features.Residentes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace home.smart.service.DataContext.Maps
{
    public class ResidenteMap : EntityMap<Residente>
    {
        public override void Configure(EntityTypeBuilder<Residente> builder)
        {
            builder.Property(t => t.Nombres).HasColumnName("Nombres").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Appellido).HasColumnName("Appellido").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.Telefono).HasColumnName("Telefono").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.Identidad).HasColumnName("Identidad").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.Correo).HasColumnName("Correo").IsUnicode(false).HasMaxLength(200);

            builder.HasOne(t => t.Localidad).WithMany(t => t.Residentes).HasForeignKey(x => x.LocalidadId);

            base.Configure(builder);
        }
    }
}