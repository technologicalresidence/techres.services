using home.smart.service.Core;
using home.smart.service.Features.LocalidadesComunitarias;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace home.smart.service.DataContext.Maps
{
    public class LocalidadComunitariaMap : EntityMap<LocalidadComunitaria>
    {
        public override void Configure(EntityTypeBuilder<LocalidadComunitaria> builder)
        {
            builder.Property(t => t.Nombre).HasColumnName("Nombre").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Ubicacion).HasColumnName("Ubicacion").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.ZonaId).HasColumnName("ZonaId");

            builder.HasOne(t => t.Zona).WithMany(t => t.LocalidadesComunitarias).HasForeignKey(x => x.ZonaId);

            base.Configure(builder);
        }
    }
}