using home.smart.service.Core;
using home.smart.service.Features.Companies;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace home.smart.service.DataContext.Maps
{
    public class CompanyMap : EntityMap<Company>
    {
        public override void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.Property(t => t.Name).HasColumnName("Name").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Description).HasColumnName("Description").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.Country).HasColumnName("Country");

            builder.HasOne(t => t.Countries).WithMany(t => t.Companies).HasForeignKey(x => x.Country);
            
            base.Configure(builder);
        }
    }
}