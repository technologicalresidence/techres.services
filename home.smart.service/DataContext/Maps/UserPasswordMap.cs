using home.smart.service.Core;
using home.smart.service.Features.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace home.smart.service.DataContext.Maps
{
    public class UserPasswordMap : EntityMap<UserPassword>
    {
        public override void Configure(EntityTypeBuilder<UserPassword> builder)
        {
            builder.Property(t => t.UserId).HasColumnName("UserId").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.PasswordHash).HasColumnName("PasswordHash").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Password).HasColumnName("Password").IsUnicode(false).HasMaxLength(200);

            builder.HasOne(t => t.User).WithMany(t => t.Passwords).HasForeignKey(x => x.UserId);

            base.Configure(builder);
        }
    }
}