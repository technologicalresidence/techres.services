using home.smart.service.Core;
using home.smart.service.Features.Zones;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace home.smart.service.DataContext.Maps
{
    public class ZonaMap : EntityMap<Zona>
    {
        public override void Configure(EntityTypeBuilder<Zona> builder)
        {
            builder.Property(t => t.Nombre).HasColumnName("Nombre").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Descripcion).HasColumnName("Descripcion").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.Direccion).HasColumnName("Direccion").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.EmpresaId).HasColumnName("EmpresaId");

            builder.HasOne(t => t.Emprea).WithMany(t => t.Zonas).HasForeignKey(x => x.EmpresaId);

            base.Configure(builder);
        }
    }
}