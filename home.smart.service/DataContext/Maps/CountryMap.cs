using home.smart.service.Core;
using home.smart.service.Features.Countries;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace home.smart.service.DataContext.Maps
{
    public class CountryMap : EntityMap<Country>
    {
        public override void Configure(EntityTypeBuilder<Country> builder)
        {
            builder.Property(t => t.Name).HasColumnName("Name").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Description).HasColumnName("Description").IsUnicode(false).HasMaxLength(200);

            base.Configure(builder);
        }
    }
}