using home.smart.service.Core;
using home.smart.service.Features.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace home.smart.service.DataContext.Maps
{
    public class UserMap : EntityMap<User>
    {
        public override void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(t => t.UserId).HasColumnName("UserId").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.FirstName).HasColumnName("FirstName").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.SecondName).HasColumnName("SecondName").IsUnicode().HasMaxLength(50);
            builder.Property(t => t.LastName).HasColumnName("LastName").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.email).HasColumnName("Email").IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.PhoneNumber).HasColumnName("PhoneNumber").IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Status).HasColumnName("Status").IsUnicode(false).IsRequired();

            base.Configure(builder);
        }
    }
}