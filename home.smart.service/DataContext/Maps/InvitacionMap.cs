using home.smart.service.Core;
using home.smart.service.Features.Invitaciones;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace home.smart.service.DataContext.Maps
{
    public class InvitacionMap : EntityMap<Invitacion>
    {
        public override void Configure(EntityTypeBuilder<Invitacion> builder)
        {
            builder.Property(t => t.CodigoInvitacion).HasColumnName("CodigoInvitacion").IsRequired().IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.Invitado).HasColumnName("Invitado").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.NumeroTelefono).HasColumnName("NumeroTelefono").IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.ResidenteId).HasColumnName("ResidenteId");
            builder.Property(t => t.VigenciaMinutos).HasColumnName("VigenciaMinutos");
            builder.Property(t => t.EstadoDeVigencia).HasColumnName("EstadoDeVigencia").IsUnicode(false).HasMaxLength(100);

            builder.HasOne(t => t.Residente).WithMany(t => t.Invitaciones).HasForeignKey(x => x.ResidenteId);

            base.Configure(builder);
        }
    }
}