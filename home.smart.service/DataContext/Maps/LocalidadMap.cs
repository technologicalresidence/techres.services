using home.smart.service.Core;
using home.smart.service.Features.Localidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace home.smart.service.DataContext.Maps
{
    public class LocalidadMap : EntityMap<Localidad>
    {
        public override void Configure(EntityTypeBuilder<Localidad> builder)
        {
            builder.Property(t => t.Nombre).HasColumnName("Nombre").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Descripcion).HasColumnName("Descripcion").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.Direccion).HasColumnName("Direccion").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.ZonaId).HasColumnName("ZonaId");

            builder.HasOne(t => t.Zona).WithMany(t => t.Localidades).HasForeignKey(x => x.ZonaId);

            base.Configure(builder);
        }
    }
}