using home.smart.service.Core;
using home.smart.service.Features.Reservas;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace home.smart.service.DataContext.Maps
{
    public class ReservaMap : EntityMap<Reserva>
    {
        public override void Configure(EntityTypeBuilder<Reserva> builder)
        {
            builder.Property(t => t.CodigoReserva).HasColumnName("CodigoReserva").IsRequired().IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.ResidenteId).HasColumnName("ResidenteId");
            builder.Property(t => t.VigenciaMinutos).HasColumnName("VigenciaMinutos");
            builder.Property(t => t.EstadoVigencia).HasColumnName("EstadoVigencia").IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.LocalidadComunitariaId).HasColumnName("LocalidadComunitariaId");

            builder.HasOne(t => t.LocalidadComunitaria).WithMany(t => t.Reservas).HasForeignKey(x => x.LocalidadComunitariaId);

            base.Configure(builder);
        } 
    }
}