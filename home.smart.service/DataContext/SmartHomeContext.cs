using home.smart.service.DataContext.Maps;
using home.smart.service.Features.Companies;
using home.smart.service.Features.Countries;
using home.smart.service.Features.Invitaciones;
using home.smart.service.Features.Localidades;
using home.smart.service.Features.LocalidadesComunitarias;
using home.smart.service.Features.Reservas;
using home.smart.service.Features.Residentes;
using home.smart.service.Features.Users;
using home.smart.service.Features.Zones;
using Microsoft.EntityFrameworkCore;

namespace home.smart.service.DataContext
{
    public class SmartHomeContext :  DbContext
    {
        public SmartHomeContext(DbContextOptions<SmartHomeContext> context) : base(context)
        {

        }

        public DbSet<Company> Companies { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Zona> Zonas { get; set; }
        public DbSet<Localidad> Localidades { get; set; }
        public DbSet<Residente> Residentes { get; set; }
        public DbSet<Invitacion> Invitaciones { get; set; }
        public DbSet<LocalidadComunitaria> LocalidadesComunitarias { get; set; }
        public DbSet<Reserva> Reservas { get; set; }
        public DbSet<User> Usuarios { get; set; }
        public DbSet<UserPassword> Passwords { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CompanyMap());
            modelBuilder.ApplyConfiguration(new CountryMap());
            modelBuilder.ApplyConfiguration(new ZonaMap());
            modelBuilder.ApplyConfiguration(new LocalidadMap());
            modelBuilder.ApplyConfiguration(new ResidenteMap());
            modelBuilder.ApplyConfiguration(new InvitacionMap());
            modelBuilder.ApplyConfiguration(new LocalidadComunitariaMap());
            modelBuilder.ApplyConfiguration(new ReservaMap());
            modelBuilder.ApplyConfiguration(new UserMap());
            modelBuilder.ApplyConfiguration(new UserPasswordMap());
        }
    }
}